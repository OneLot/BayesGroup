Bayes Group is a back end solution for UR start-up using some [fat jar la imports](https://github.com/thomaxxl/safrs) <br>

The tech stack is Flask(Python), Apache(C), SqlAlchemy(ORM) and Restful service. Swagger front-end is React JS + Redux apparently to show you what functions the back end can be called for end-clients to CRUD (create, read, update, delete).<br>

The project is served using a reverse proxy (NgInx) on one core cpu. <br>

Run Local
```json
{
  "1st-command": "cd BayesGroup",
  "2nd-command": "source bin/bubbaenv/bin/activate",
  "3rd-command": "python bin/demo_bayes_group.py 127.0.0.1 5000 http"
}
```

Run Remote 
```
injects through wsgi.py with .ini from NgInx setup
```
