import datetime

from .base import BaseModel, db


class Job(BaseModel):
    """
        description: Job is position Candidates Hold or Want to Target Hodl inside Company domain model
    """

    __tablename__ = "Jobs"
    id = db.Column(db.String, primary_key=True)
    title = db.Column(db.String, default="")
    held_jobs_id = db.Column(db.Integer, db.ForeignKey("Candidates.id"))
    target_jobs_id = db.Column(db.Integer, db.ForeignKey("Candidates.id"))
    company_id = db.Column(db.Integer, db.ForeignKey("Companies.id"))
    company: object = db.relationship("Company", back_populates="jobs", cascade="save-update, delete")
    as_of = db.Column(db.DateTime, default=datetime.datetime.now(), primary_key=True)
