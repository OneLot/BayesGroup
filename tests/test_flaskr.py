import unittest

class TestApp(unittest.TestCase):

    def setUp(self) -> None:
        self.test_host = "120.0.0.1"
        self.test_port = 5000

    def test_empty_db(client):
        """Start with a blank database."""
        assert '1' in ['1','2']