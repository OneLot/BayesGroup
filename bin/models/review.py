import datetime

from .base import db, BaseModel


class Review(BaseModel):
    """
        description: Sticky notes for a candidate
    """

    __tablename__ = "Reviews"
    candidate_id = db.Column(db.Integer, db.ForeignKey("Candidates.id", ondelete="CASCADE"), primary_key=True)
    review_notes = db.Column(db.String, default="")
    created = db.Column(db.DateTime, default=datetime.datetime.now(), primary_key=True)
    http_methods = {"GET", "POST"}  # only allow GET and POST
