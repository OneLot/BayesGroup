from flask_sqlalchemy import SQLAlchemy
from safrs import SAFRSBase  # db Mixin
from safrs.api_methods import search  # rpc methods

db = SQLAlchemy()


class BaseModel(SAFRSBase, db.Model):
    __abstract__ = True
    # Add startswith methods so we can perform lookups from the frontend
    SAFRSBase.search = search
    # Needed because we don't want to implicitly commit when using flask-admin
    SAFRSBase.db_commit = False


class DocumentedColumn(db.Column):
    """
        The class attributes are used for the swagger
    """

    description = "My custom column description"
    swagger_type = "string"
    swagger_format = "string"
    name_format = "filter[{}]"  # Format string with the column name as argument
    required = False
    default_filter = ""
    sample = "my custom value"


# SQLA models that will be exposed
# friendship = db.Table(
#     "friendships",
#     db.metadata,
#     db.Column("friend_a_id", db.Integer, db.ForeignKey("People.id"), primary_key=True),
#     db.Column("friend_b_id", db.Integer, db.ForeignKey("People.id"), primary_key=True),
# )


def hidden_relationship(*args, **kwargs):
    """
        To hide a relationship, set the expose attribute to False
    """
    relationship = db.relationship(*args, **kwargs)
    relationship.expose = False
    return relationship
