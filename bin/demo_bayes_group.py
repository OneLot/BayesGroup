# !/usr/bin/env python3

import sys
from pathlib import Path

from flask import Flask, redirect, send_from_directory
from flask_admin import Admin
from flask_admin.contrib import sqla
from flask_cors import CORS
from safrs import SAFRSAPI  # api factory

from models import db, Job, Candidate, Company, Review

description = """
<a href=http://jsonapi.org>Json:API</a> compliant API built with https://github.com/thomaxxl/safrs <br/>
- <a href="/ja/index.html">reactjs+redux frontend</a>
- <a href="/admin/person">Flask-Admin frontend</a>
- <a href="/swagger_editor/index.html?url=/api/swagger.json">Swagger2 Editor</a> (updates can be added with the SAFRSAPI "custom_swagger" argument)
"""


# API app initialization:
# Create the instances and exposes the classes
def start_api(swagger_host="0.0.0.0", PORT=None, isProd=False):
    with app.app_context():
        if isProd:
            schemes = ["https"]
            app.config.update(SQLALCHEMY_DATABASE_URI="sqlite:///bayes_group_lite_prod.db", DEBUG=False)
            db.init_app(app)
            db.load_only()
        else:
            schemes = ["http"]
            app.config.update(SQLALCHEMY_DATABASE_URI="sqlite:///bayes_group_lite_dev.db", DEBUG=True)
            db.init_app(app)
            db.create_all()
            NR_INSTANCES = 2
            for i in range(NR_INSTANCES):
                job_seeker = Candidate(name="Seeker-{}".format(i), contact_email="seeker-{}@email.com".format(i), contact_phone="+852 9617 701{}".format(i), _password=str(i))
                job_holder = Candidate(name="Hodler-{}".format(i), contact_email="holder-{}@email.com".format(i), contact_phone="+852 9617 701{}".format(i + 1), _password=str(i))
                job = Job(title="JobTitle-{}".format(i))
                review = Review(candidate_id=2 * i + 1, review_notes=f"review {i}")
                company = Company(name="Company-{} LLC.".format(i))
                company.jobs.append(job)
                job_seeker.held_jobs.append(job)
                job_holder.target_jobs.append(job)
                if i % 20 == 0:
                    job_seeker.comment = ""
                for obj in [job_holder, job, company, review, job_seeker]:
                    db.session.add(obj)
                db.session.commit()

        custom_swagger = {
            "info": {"title": "BayesGroup back-end API"},
            "securityDefinitions": {"ApiKeyAuth": {"type": "apiKey", "in": "header", "name": "My-ApiKey"}},
        }  # Customized swagger will be merged
        api = SAFRSAPI(
            app,
            host=swagger_host,
            port=PORT,
            prefix=API_PREFIX,
            custom_swagger=custom_swagger,
            schemes=schemes,
            description=description,
        )

        for model in [Candidate, Job, Review, Company]:
            # Create an API endpoint
            api.expose_object(model)

        # add the flask-admin views
        admin = Admin(app, url="/admin")
        for model in [Candidate, Job, Review, Company]:
            admin.add_view(sqla.ModelView(model, db.session))


app = Flask(__name__)


# app routes
@app.route("/ja")  # React jsonapi frontend
@app.route("/ja/<path:path>", endpoint="jsonapi_admin")
def send_ja(path="index.html"):
    return send_from_directory(Path(__file__).parent.parent / "jsonapi-admin/build", path)


@app.route("/swagger_editor/<path:path>", endpoint="swagger_editor")
def send_swagger_editor(path="index.html"):
    return send_from_directory(Path(__file__).parent.parent / "swagger-editor", path)


@app.route("/")
def goto_api():
    return redirect(API_PREFIX)


app.secret_key = "not so secret"
cors = CORS(app, resources={r"/api/*": {"origins": "*"}})
API_PREFIX = "/api"
HOST = "bayeswagyu.com"
PORT = 443

if __name__ == "__main__":
    if len(sys.argv) > 1:
        HOST = sys.argv[1]
    if len(sys.argv) > 2:
        PORT = int(sys.argv[2])
    print('starting dev')
    start_api(HOST, PORT, isProd=False)
    app.run(host=HOST, port=PORT, threaded=False, debug=True)
else:
    print('starting prod')
    start_api(HOST, PORT, isProd=True)
