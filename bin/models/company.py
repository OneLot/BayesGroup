from typing import List

from safrs import jsonapi_attr, SAFRSBase
from .base import BaseModel, db, hidden_relationship
from .candidate import Candidate


class Company(BaseModel):
    """
        description: Company are the domain model that pay to employ candidate
        ---
        demonstrate custom (de)serialization in __init__ and to_dict
    """

    __tablename__ = "Companies"
    id = db.Column(db.Integer, primary_key=True)  # Integer pk instead of str
    title = db.Column(db.String, default="")
    # Jobs = db.relationship("Job", back_populates="publisher", lazy="dynamic")
    jobs: List = db.relationship("Job", back_populates="company")
    employees = hidden_relationship(Candidate, back_populates="employer")

    def __init__(self, *args, **kwargs):
        custom_field = kwargs.pop("custom_field", None)
        SAFRSBase.__init__(self, **kwargs)

    def to_dict(self):
        result = SAFRSBase.to_dict(self)
        result["custom_field"] = "some customization"
        return result

    @classmethod
    def filter(cls, arg):
        """
            Sample custom filtering, override this method to implement custom filtering
            using the sqlalchemy orm
            This method will be called when the filter= url query argument is provided, eg.
            GET /Companies/?filter=some_filter
            you can use the argument to create custom filtering, f.i.
            cls.query.filter_by(name=arg)
        """
        print(arg)
        return {"provided": arg}

    @jsonapi_attr
    def stock(self):
        """
            default: 30
            ---
            Custom Attribute that will be shown in the Job swagger
        """
        return 100
