import hashlib

from safrs import SAFRSFormattedResponse, jsonapi_rpc, jsonapi_attr

from .base import BaseModel, DocumentedColumn, db, hidden_relationship
from .job import Job


class Candidate(BaseModel):
    """
        description: Candidate are persons that are sought after (held_jobs) to Company or seeking (target_jobs) to Company domain model
    """
    __tablename__ = "Candidates"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String, default="John Doe")
    comment = DocumentedColumn(db.Text, default="Empty comment")
    dob = db.Column(db.Date)
    experience_in_job = db.Column(db.Integer, default=0)
    experience_in_industry = db.Column(db.Integer, default=0)

    contact_phone = db.Column(db.String, default="")
    contact_email = db.Column(db.String, default="")
    linked_in_url = db.Column(db.String, default="linkedin.com")

    held_jobs = db.relationship("Job", backref="employee", foreign_keys=[Job.held_jobs_id], cascade="save-update, merge")
    target_jobs = db.relationship("Job", backref="potential_employee", foreign_keys=[Job.target_jobs_id])
    reviews = db.relationship("Review", backref="reader", cascade="save-update, delete")
    employer_id = db.Column(db.Integer, db.ForeignKey("Companies.id"))
    employer = hidden_relationship("Company", back_populates="employees", cascade="save-update, delete")
    _password = db.Column(db.String, default="")

    @jsonapi_attr
    def password(self):
        """---
            "_password" is hidden because of the "_" prefix, provide a custom attribute "password" the Person fields
        """
        return "hidden, check _password"

    @password.setter
    def password(self, val):
        """
            Allow setting _password
        """
        self._password = hashlib.sha256(val.encode("utf-8")).hexdigest()

    # Following methods are exposed through the REST API
    @jsonapi_rpc(http_methods=["POST"])
    def send_mail(self, email=""):
        """
            description : Send an email
            args:
                email: test email
            parameters:
                - name : my_query_string_param
                  default : my_value
        """
        content = "Mail to {} : {}\n".format(self.name, email)
        with open("/tmp/mail.txt", "a+") as mailfile:
            mailfile.write(content)
        return {"output": "sent {}".format(content)}

    @classmethod
    @jsonapi_rpc(http_methods=["POST"])
    def my_rpc(cls, *args, **kwargs):
        """
            pageable: false
            parameters:
                - name : my_query_string_param
                  default : my_value
            args:
                email: test email
        """
        o1 = cls.query.first()
        o2 = cls.query.first()
        o1.friends.append(o2)
        data = [o1, o2]
        # build a jsonapi response object
        response = SAFRSFormattedResponse(data, {}, {}, {}, 1)
        return response
